\documentclass[a4paper,10pt]{article}
\newcommand{\ds}{\displaystyle}
\usepackage{amssymb,amsmath,graphicx,wrapfig,verbatim, psfragx,color}
\usepackage[margin=0.5in]{geometry}
\def\FillInBlank{\rule{1truein} {.01truein}}
\def\ds{\displaystyle}
\begin{document}
\vspace{-0.5in}
\begin{center}
 % Math 221 - Week 1 - Worksheet 2\\
 Topics: Section 1.5 - The Limit of a Function 
\end{center}
\vspace{.2in}


\noindent {\bf Instructions:} Listen to your TA's instructions. There are substantially more problems on this worksheet than we expect to be done in discussion, and your TA might not have you do problems in order. The worksheets are intentionally longer than will be covered in discussion in order to give students additional practice problems they may use to study. Do not worry if you do not finish the worksheet :). 

\medskip


\begin{enumerate}
\item Make an educated guess of the value of the following limits.  

\begin{enumerate}
\item $\ds\lim_{s \to 5} s -3.$
\item $\ds\lim_{u \to -2} u^2 - \cos(\pi u).$ 
\item $\ds\lim_{v \to 4} \dfrac{v+3}{4v-2}.$ 
\end{enumerate}
\paragraph{Solution.} These are all continuous functions. Functions (a) and (b) are continuous everywhere; the functions' graphs never have any jumps or blow-ups. The third function is continuous except where it's not defined when $4v-2=0$, which happens when $v=\frac{1}{2}$. This is not a problem here because we're taking the limit as $v$ moves towards $4$. So in each case, the limit is the same as the value of the function with $s=5$, $u=-2$, $v=4$ plugged in, respectively.
\begin{enumerate}
\item $\ds\lim_{s \to 5} s -3 = 5-3 = 2$.
\item $\ds\lim_{u \to -2} u^2 - \cos(\pi u) = (-2)^2 - \cos(-2\pi) = 4-1 = 3.$ 
\item $\ds\lim_{v \to 4} \dfrac{v+3}{4v-2} = \frac{4+3}{4(4)-2} = \frac{7}{14} = \frac{1}{2}.$ 
\end{enumerate}

\bigskip 


\item Sketch the graph of an example of a function $f$ that satisfies all of the following: $\ds\lim_{x \to -3^-} f(x) = 2,$ $\ds\lim_{x \to -3^+} f(x) = 2,$ $\ds\lim_{x \to 1^-} f(x) = 4,$ $\ds\lim_{x \to 1^+} f(x) = -1,$ $f(-3) =  4,$ $f(1)=-1.$ 
\paragraph{Solution.} Here's an example. The blue circles represent holes and the red circles represent function values.
\begin{center}
	\includegraphics[scale=.29]{./soln-graph01.png}
\end{center}
\newpage

\item Determine the infinite limit. 
\begin{enumerate}
\item $\ds\lim_{s \to 1^-} \dfrac{s^2-4}{s-1}.$ 
\paragraph{Solution.} The numerator approaches $(-1)^2-4=-3$. The denominator approaches $0$ from the left-hand side, where the function $g(s)=s-1$ is negative. Therefore the limit is positive infinity (a negative divided by a negative is positive).
\[ \ds\lim_{s \to 1^-} \dfrac{s^2-4}{s-1} = +\infty .\]
\item $\ds\lim_{u \to 3^+} \dfrac{u^2-2u-8}{u^2-6u+9}.$
\paragraph{Solution.} Factoring the numerator and denominator is usually a good idea.
\[ \ds\lim_{u \to 3^+} \dfrac{u^2-2u-8}{u^2-6u+9} = \ds\lim_{u \to 3^+} \dfrac{(u-4)(u+2)}{(u-3)^2} . \]
Now we can see that the numerator approaches $(3-4)(3+2) = -5$ as $u$ moves towards $3$ from the right, and the denominator approaches $0$ and is always positive (a square is always positive). Hence, the limit is negative infinity (negative divided by positive is negative).
\[ \ds\lim_{u \to 3^+} \dfrac{u^2-2u-8}{u^2-6u+9} = -\infty .\]
\item $\ds\lim_{t \to 9^-} \dfrac{\sqrt{t}}{(t-9)^3}.$ 
\paragraph{Solution.} The numerator approaches $\sqrt{9}=3$ and the denominator approaches $0$ through negative values ($t-9$ is negative to the left of $t=9$, and therefore so is $(t-9)^3$). So the limit is $-\infty$.\\

\item $\ds\lim_{\theta \to \pi^+} \dfrac{\theta-4}{\sin(\theta)}$
\paragraph{Solution.} Similar, the answer is $+\infty$. At $\theta=\pi$, we have $\theta-4=\pi-4 < 0$, and $\sin(\theta)$ is negative when $\theta$ is near $\pi$ on the right-hand side (draw the graph). A negative divided by a negative is positive.\\
\end{enumerate} 

\newpage


% \newpage

\item Consider the function $f(x) = \dfrac{2x-3}{(x-2)(x+4)}.$ 
\begin{enumerate}
\item Find all the vertical asymptotes of $f.$
\paragraph{Solution.} The denominator is zero when $x=2$ and $x=-4$. The numerator is zero when $x=\frac{3}{2}$, which does not duplicate any of the denominator's zeros. Therefore there are vertical asymptotes at $x=2$ and $x=-4$.\\
\item Compute $\ds\lim_{x \to 2^+}f(x),$ $\ds\lim_{x \to 2^-}f(x),$ $\ds\lim_{x \to -4^+}f(x),$ and $\ds\lim_{x \to -4^-}f(x).$ 
\paragraph{Solution.} They are $+\infty$, $-\infty$, $+\infty$, $-\infty$, based on analyzing which terms are positive or negative as we move from the left-hand or right-hand side.
\item Make a rough sketch of the function. \\
\paragraph{Solution.} Red lines represent vertical asymptotes.
\begin{center}
	\includegraphics[scale=.29]{./soln-graph02.png}
\end{center}
\end{enumerate}


\item Consider the functions $f(x) = x+2,$ $g(x) = \dfrac{(x-3)(x+2)}{x-3},$ and $h(x) = \begin{cases}  \dfrac{(x-3)(x+2)}{x-3} & x \neq 3 \\ 8 & x=3.\\ \end{cases}.$ Sketch each of the functions. Then determine the limit as $x \to 3$ of each of the functions. If the limit does not exist, state so. 
\paragraph{Solution.} $\lim_{x\rightarrow 3} f(x) = 3+2 = 5$, since $f(x)$ is continuous everywhere.
We find that $g(x)$ simplifies to $g(x) = x+2$ with a hole at $x=3$. So we can find the limit $\lim_{x\rightarrow 3} g(x) = 3+2 = 5$. Likewise, we ignore the jump of $h(x)$ at $x=3$ and find $\lim_{x\rightarrow 3} h(x) = 3+2=5$.\\


\newpage
\item Below is the graph of $g(t).$ For each of the given points determine the value of $g(a),$ $\ds\lim_{t\to a^-} g(t),$ $\ds\lim_{t\to a^+} g(t),$ and $\ds\lim_{t\to a} g(t).$ If any of the quantities do not exist, explain why.  
\begin{center}
\includegraphics[width=0.5\textwidth]{onesidedlimits}
\end{center}
\paragraph{Solution.} A limit does not exist when the corresponding right-hand and left-hand limits are not equal.
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
\hline
$a$ & $g(a)$       & $\lim_{t\rightarrow a-}g(t)$ & $\lim_{t\rightarrow a+}g(t)$ & $\lim_{t\rightarrow a}g(t)$ \\ \hline
-4  & 3            & 3                            & -2                           & Does not exist.             \\ \hline
-1  & 4            & 4                            & 4                            & 4                           \\ \hline
2   & -1           & -1                           & 5                            & Does not exist.             \\ \hline
4   & Not defined. & 2                            & 2                            & 2                           \\ \hline
\end{tabular}
\end{center}

\item Sketch the graph of the function and use it to determine the values of $a$ for which $\lim_{x \to a} f(x) $ exists. 

$$ f(x) = \begin{cases} 
      3+x, & x<-2 \\
      x^2-2, & -2\le x\le 3 \\
      10-x, & x>3. 
   \end{cases}
$$

\paragraph{Solution.}
 \begin{center}
	\includegraphics[scale=.29]{./soln-graph03.png}
\end{center}
From the picture, we see that the second and third pieces of the function are glued together seamlessly at $x=3$, so $\lim_{x\rightarrow 3} f(x)$ exists and is equal to $7$. The limit of $f(x)$ at $x=-2$ does not exist since there is a jump discontinuity at $x=-2$. The function $f(x)$ is continuous at all other values of $x$.


\newpage


\item Consider the function $f(x) = \tan\left( \frac{1}{x} \right). $
\begin{enumerate}
\item Show that $f(x) = 0$ for $x= \frac{1}{\pi}, \frac{1}{2\pi}, \frac{1}{3\pi}, \ldots.$
\paragraph{Solution.} Plugging in these values of $x$, we have $f(1/\pi) = \tan(\pi) = 0$, $f(1/2\pi) = \tan(2\pi) = 0$, and so on.\\
\item Show that $f(x) = 1$ for $x= \frac{4}{\pi}, \frac{4}{5\pi}, \frac{4}{9\pi}, \ldots.$
\paragraph{Solution.} Likewise, $f(4/\pi) = \tan(\frac{1}{4/\pi}) = \tan(\frac{\pi}{4}) = 1$, and so on.\\
\item What can you conclude about $\ds\lim_{x \to 0^+} \tan \left(\frac{1}{x} \right)?$
\paragraph{Solution.} We have found two sequences of numbers approaching $x=0$ from the right where $f(x)$ approaches two different values, 0 and 1. This means that $\lim_{x\rightarrow 0+} \tan(1/x)$ does not exist.
\end{enumerate}


\end{enumerate}


\end{document}
