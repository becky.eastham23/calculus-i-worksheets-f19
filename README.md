# Calculus I Worksheets - Fall 2019

Solutions for Calculus I worksheets. Fall 2019 semester at the University of Wisconsin-Madison. 😴

Graphs are made using [GNU Octave](https://www.gnu.org/software/octave/). The scripts that compile the graphs should run in MATLAB also.
