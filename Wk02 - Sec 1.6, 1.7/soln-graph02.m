f = @(t) t.^3;
x = -3:.05:3;


plot(x,f(x),'linewidth',4,'b');
hold on
plot(x,zeros(1,length(x)),'linewidth',1,'k')
hold on
plot([-3 3],[0 0],'linewidth',1,'k')
hold on
plot([-3 3],[8 8],'linewidth',1,'r')
hold on
plot([-3 3],[-8 -8],'linewidth',1,'r')
hold on
plot([0 0],[-30 30],'linewidth',1,'k')
hold on
scatter([-2 2],[-8 8],100,'filled','c')


axis([-3 3 -12 12])